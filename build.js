#!/usr/bin/env node
import estrella from 'estrella'
import fsExtra from 'fs-extra'
import chalk from 'chalk'
import { dirname } from 'path'
import { fileURLToPath } from 'url'

const __dirname = dirname(fileURLToPath(import.meta.url))

const { build, watch } = estrella
const { remove, copy, writeFile } = fsExtra

build({
  entry: __dirname + '/src/index.ts',
  bundle: true,
  outdir: __dirname + '/dist',
  minify: false,
  external: ['__STATIC_CONTENT_MANIFEST'],
  outExtension: { '.js': '.mjs' },
  format: 'esm',
  onStart: async (config, changedFiles, context) => {
    const isInitialBuild = changedFiles.length === 0
    if (isInitialBuild) {
      try {
        await copy('static', 'dist/static', { recursive: true })
      } catch (e) {
        console.warn(
          chalk.yellow(
            'Could not remove existing dist folder and copy static assets (maybe you are running wrangler dev?)'
          )
        )
      }
    }
  },
})
