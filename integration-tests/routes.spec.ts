import axios from 'axios'
import { Game, GameRankingModel, ModeRankingModel } from '@tgmrank/types'

describe('routes', () => {
  it('should return json for invalid route', async () => {
    const response = await axios
      .get(`${process.env.GATEWAY_URL}/some/path`)
      .catch((e) => e.response)

    expect(response).not.toBeNull()
    expect(response.status).toBe(404)
    expect(response.data).not.toBeNull()

    expect(response.data.message).toBe('Not found')
  })

  it('should get games', async () => {
    const response = await axios.get(`${process.env.GATEWAY_URL}/v1/game`)

    expect(response.status).toBe(200)
    expect(response.data).not.toBeNull()

    const games: ReadonlyArray<Game> = response.data
    expect(games[0].gameName).toBe('Overall')
  })

  it('should get locations', async () => {
    const response = await axios.get(`${process.env.GATEWAY_URL}/v1/game`)

    expect(response.status).toBe(200)
    expect(response.data).not.toBeNull()
  })

  describe('aggregate ranking', () => {
    it('should get overall aggregate ranking', async () => {
      const response = await axios.get(
        `${process.env.GATEWAY_URL}/v1/game/0/main/ranking`
      )

      expect(response.status).toBe(200)
      expect(response.data).not.toBeNull()

      const ranking: ReadonlyArray<GameRankingModel> = response.data
      expect(ranking.length).toBeGreaterThan(0)
    })

    it('should get overall extended aggregate ranking', async () => {
      const response = await axios.get(
        `${process.env.GATEWAY_URL}/v1/game/0/extended/ranking`
      )

      expect(response.status).toBe(200)
      expect(response.data).not.toBeNull()

      const ranking: ReadonlyArray<GameRankingModel> = response.data
      expect(ranking.length).toBeGreaterThan(0)
    })

    it('should return an error given an invalid ranking key', async () => {
      await expect(
        axios.get(
          `${process.env.GATEWAY_URL}/v1/game/0/bad-ranking-key/ranking`
        )
      ).rejects.toThrow()
    })
  })

  describe('mode ranking', () => {
    it('should get mode ranking', async () => {
      const response = await axios.get(
        `${process.env.GATEWAY_URL}/v1/mode/1/ranking`
      )

      expect(response.status).toBe(200)
      expect(response.data).not.toBeNull()

      const ranking: ReadonlyArray<ModeRankingModel> = response.data
      expect(ranking.length).toBeGreaterThan(0)
    })
  })

  describe('get player scores', () => {
    it('should get player scores', async () => {
      const response = await axios.get(
        `${process.env.GATEWAY_URL}/v1/player/142/scores`
      )

      expect(response.status).toBe(200)
      expect(response.data).not.toBeNull()

      const scores: ReadonlyArray<ModeRankingModel> = response.data
      expect(scores.length).toBeGreaterThan(0)
    })
  })
})
