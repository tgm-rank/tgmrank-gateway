/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
export default {
  preset: 'ts-jest/presets/default-esm',
  globals: {
    'ts-jest': {
      tsconfig: 'tsconfig.unit.json',
      useESM: true,
    },
  },
  setupFiles: ['dotenv/config'],
  testMatch: ['**/integration-tests/*.spec.ts'],
  testEnvironment: 'node',
}
