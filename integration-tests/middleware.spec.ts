import axios from 'axios'

describe('middleware', () => {
  it('should set Vary headers', async () => {
    const response = await axios.get(`${process.env.GATEWAY_URL}/v1/game`)
    const headers = response.headers
    expect(headers['vary']).toBe(
      'Accept-Encoding,Origin,Access-Control-Request-Method,Access-Control-Request-Headers'
    )
  })

  describe('cors', () => {
    const someValidOrigin =
      process.env.CORS_ALLOWED_ORIGINS?.split(';')?.[0] ?? ''

    it('should set cors headers for expected error responses', async () => {
      const response = await axios.options(
        `${process.env.GATEWAY_URL}/some/path`,
        {
          headers: {
            Origin: someValidOrigin,
            'Access-Control-Request-Method': 'DELETE',
          },
        }
      )

      expect(response).not.toBeNull()
      expect(response.status).toBe(204)
      expect(response.headers['access-control-allow-origin']).toBe(
        someValidOrigin
      )
    })

    it('should reject invalid origin', async () => {
      const response = await axios
        .options(`${process.env.GATEWAY_URL}/some/path`, {
          headers: {
            Origin: 'http://my-invalid-origin',
            'Access-Control-Request-Method': 'POST',
          },
        })
        .catch((e) => e.response)

      expect(response.status).toBe(400)
    })

    it('should handle preflight requests', async () => {
      const response = await axios
        .get(`${process.env.GATEWAY_URL}/some/path`, {
          headers: {
            Origin: someValidOrigin,
          },
        })
        .catch((e) => e.response)

      expect(response).not.toBeNull()
      expect(response.headers['access-control-allow-origin']).toBe(
        someValidOrigin
      )
    })
  })
})
