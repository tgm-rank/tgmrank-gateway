import { jest } from '@jest/globals'

import { Context } from '@/bindings'
import type { KV } from 'worktop/cfw.kv'
import { mockDeep } from 'jest-mock-extended'

export const testEnv = {
  tgmrankData: mockDeep<KV.Namespace>(),

  TGMRANK_API_URL: 'https://tgmrank.api',
  CORS_ALLOWED_ORIGINS: 'https://my-origin.site;https://my-other-origin.site',
}

export function createContext(): Context {
  return {
    bindings: testEnv,
    waitUntil: jest.fn(),
    passThroughOnException: jest.fn(),
    defer: jest.fn(),
  } as unknown as Context
}
