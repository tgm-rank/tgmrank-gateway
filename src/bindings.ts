import type * as worktop from 'worktop'
import type { KV } from 'worktop/cfw.kv'
import { Environment } from './types/Environment'

export interface Context extends worktop.Context {
  bindings: {
    tgmrankData: KV.Namespace
    ENVIRONMENT: Environment
    TGMRANK_API_URL: string
    CORS_ALLOWED_ORIGINS: string
    FIGHTCADE_API_URL: string
  }
}

export type Handler = worktop.Handler<Context>
