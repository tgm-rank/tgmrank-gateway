import { defaultExpiration, getOrInsert } from '@/api'
import { Context } from '@/bindings'
import {
  FightcadeRankingResponse,
  FightcadeUserMappingEntry,
} from '@/types/FightcadeRankingResponse'
import { kvPath } from '@/utility'
import { Player } from '@tgmrank/types'

const FightcadeRawRankingKey = 'tgmrank-gateway-fightcade-ranking-raw'
const FightcadeUserMappingKey = 'tgmrank-gateway-fightcade-user-mapping'
const PlayersList = '/v1/players'

enum FightcadeGameId {
  tgm2p = 'tgm2p',
}

async function fetchPage(
  apiUrl: string,
  gameId: FightcadeGameId,
  offset: number,
  pageSize: number
) {
  return fetch(apiUrl, {
    method: 'POST',
    body: JSON.stringify({
      req: 'searchrankings',
      offset: offset,
      limit: pageSize,
      gameid: gameId,
      byElo: true,
      recent: true,
    }),
  })
}

export async function getLeaderboard(
  ctx: Context,
  gameId: FightcadeGameId = FightcadeGameId.tgm2p
) {
  const pageSize = 10
  const pagePromises: Array<Promise<FightcadeRankingResponse>> = []

  for (let i = 0; i < pageSize * 10; i += pageSize) {
    pagePromises.push(
      fetchPage(ctx.bindings.FIGHTCADE_API_URL, gameId, i, pageSize).then((r) =>
        r.json<FightcadeRankingResponse>()
      )
    )
  }

  const pages =
    (await getOrInsert<Array<FightcadeRankingResponse>>(
      async () =>
        ctx.bindings.tgmrankData.get(FightcadeRawRankingKey, {
          cacheTtl: defaultExpiration.getTtl,
          type: 'json',
        }),
      async () =>
        ctx.bindings.tgmrankData.put(
          FightcadeRawRankingKey,
          await Promise.all(pagePromises).then((res) => JSON.stringify(res)),
          {
            expirationTtl: 3600,
          }
        )
    )) ?? []

  const userMapping: ReadonlyArray<FightcadeUserMappingEntry> =
    (await ctx.bindings.tgmrankData.get(FightcadeUserMappingKey, 'json')) ?? []

  const playerList: ReadonlyArray<Player> =
    (await ctx.bindings.tgmrankData.get(
      kvPath(ctx.bindings.ENVIRONMENT, PlayersList),
      'json'
    )) ?? []

  const ranks = ['F', 'E', 'D', 'C', 'B', 'A', 'S']
  const combinedResults = pages
    .flatMap((p) => p.results.results)
    .map((r) => ({
      ...r,
      rank: ranks[r.gameinfo.tgm2p.rank],
      player: playerList.find(
        (p) =>
          p.playerId ===
          userMapping.find((user) => user.fcUsername === r.name)?.userId
      ) ?? {
        playerName: r.name,
        location: r.country?.iso_code?.toLowerCase(),
      },
    }))

  return combinedResults
}
