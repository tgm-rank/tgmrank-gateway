import { Router } from 'worktop'

import { corsMiddleware } from './middleware/cors'
import { varyHeader } from './middleware/vary'
import { registerRoutes } from './routes'
import { cachePlayerScores, scheduledCacher } from './schedule'

import { Context } from './bindings'
import { start } from 'worktop/cfw'

const api = new Router<Context>()

api.prepare = async (request: Request, ctx: Context) => {
  varyHeader(ctx)

  const corsResponse = corsMiddleware(request, ctx)
  if (corsResponse != null) {
    return corsResponse
  }
}
registerRoutes(api)

async function scheduleHandler(
  event: ScheduledEvent,
  env: unknown,
  cfCtx: ExecutionContext
) {
  const ctx: Context = {
    bindings: env,
    waitUntil: cfCtx.waitUntil.bind(cfCtx),
    passThroughOnException: cfCtx.passThroughOnException.bind(cfCtx),
  } as Context

  switch (event.cron) {
    // You can set up to three schedules maximum.
    case '*/30 * * * *':
      ctx.waitUntil(scheduledCacher(ctx))
      break
    case '*/5 * * * *':
      ctx.waitUntil(cachePlayerScores(ctx))
      break
  }
}

export default {
  ...start(api.run),
  scheduled: scheduleHandler,
}
