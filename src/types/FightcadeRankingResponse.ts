export interface FightcadeRankingResponse {
  results: FightcadeResults
  res: string
}

export interface FightcadeResults {
  results: FightcadeResult[]
  count: number
}

export interface FightcadeResult {
  name: string
  country: Country
  gameinfo: Gameinfo
}

export interface Country {
  iso_code: string
  full_name: string
}

export interface Gameinfo {
  tgm2p: Tgm2P
}

export interface Tgm2P {
  num_matches: number
  time_played: number
  rank: number
}

export interface FightcadeUserMappingEntry {
  fcUsername: string
  userId: number
}
