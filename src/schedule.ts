import { Player } from '@tgmrank/types'
import { storeResponse } from './api'
import { Context } from './bindings'
import { circularSlice } from './utility'
import { PromisePool } from '@supercharge/promise-pool'

enum EventTypes {
  StaticData = 0,
  AggregateRankings = 1,
}

const EventTypesCount = Object.keys(EventTypes).length / 2

const ScheduleRotationKey = 'tgmrank-gateway-schedule-rotation-key'
const SchedulePlayerScoresRotationKey =
  'tgmrank-gateway-schedule-player-scores-rotation-key'

export async function scheduledCacher(ctx: Context) {
  const rotationKey = Number(
    (await ctx.bindings.tgmrankData.get(ScheduleRotationKey)) ?? '0'
  )

  switch (rotationKey % EventTypesCount) {
    case EventTypes.StaticData:
      {
        const paths = [
          '/v1/game',
          '/v1/location',
          '/v1/players/ranked',
          '/v1/players',
        ]
        await cacheResponses(paths, ctx)
      }
      break
    case EventTypes.AggregateRankings:
      {
        const gameIds = [0, 1, 2, 3]
        const rankingKeys = ['main', 'extended']

        const paths = []
        for (const gameId of gameIds) {
          for (const r of rankingKeys) {
            paths.push(`/v1/game/${gameId}/${r}/ranking`)
          }
        }

        await cacheResponses(paths, ctx)
      }
      break
  }

  await ctx.bindings.tgmrankData.put(
    ScheduleRotationKey,
    (rotationKey + 1).toString()
  )
}

export async function cachePlayerScores(ctx: Context) {
  await cacheResponses(['/v1/players/ranked'], ctx)

  const batchSize = 10

  const rankedPlayerListOffset = Number(
    (await ctx.bindings.tgmrankData.get(SchedulePlayerScoresRotationKey)) ?? '0'
  )

  const rankedPlayerList: ReadonlyArray<Player> =
    (await ctx.bindings.tgmrankData.get('/v1/players/ranked', 'json')) ?? []

  const playersToUpdate = circularSlice(
    rankedPlayerList,
    rankedPlayerListOffset,
    batchSize
  )

  const paths = playersToUpdate.map((p) => `/v1/player/${p.playerId}/scores`)

  await cacheResponses(paths, ctx)

  await ctx.bindings.tgmrankData.put(
    SchedulePlayerScoresRotationKey,
    ((rankedPlayerListOffset + batchSize) % rankedPlayerList.length).toString()
  )
}

async function cacheResponses(paths: string[], ctx: Context) {
  ctx.waitUntil(
    PromisePool.withConcurrency(3)
      .for(paths)
      .process(async (path: string) => storeResponse(ctx, path, undefined))
  )
}
