import { KV } from 'worktop/cfw.kv'
import { Context } from './bindings'
import { kvPath } from './utility'

export interface Expiration {
  getTtl: number
  putTtl: number | undefined
}

export const defaultExpiration: Expiration = {
  getTtl: 60,
  putTtl: undefined,
}

export async function storeResponse(
  ctx: Context,
  path: string,
  expirationTtl: number | undefined
): Promise<ReadableStream | null> {
  const apiUrl = `${ctx.bindings.TGMRANK_API_URL}${path}`
  const apiResponse = await fetch(apiUrl)

  if (apiResponse.status != 200 || apiResponse.body == null) {
    return null
  }

  const [streamForCache, streamForResponse] = apiResponse.body.tee()

  await ctx.bindings.tgmrankData.put(
    kvPath(ctx.bindings.ENVIRONMENT, path),
    streamForCache,
    {
      expirationTtl,
    }
  )

  return streamForResponse
}

export async function getOrInsert<T>(
  get: () => Promise<T | null>,
  put: () => Promise<unknown>
): Promise<T | null> {
  const getResult = await get()

  if (getResult) {
    return getResult
  }

  await put()
  return await get()
}

export async function readOrCache(
  ctx: Context,
  key: string,
  action: () => Promise<KV.Value | Response>,
  expiration: Expiration = defaultExpiration
): Promise<KV.Value> {
  const fromCache = await ctx.bindings.tgmrankData.get(
    kvPath(ctx.bindings.ENVIRONMENT, key),
    {
      cacheTtl: expiration.getTtl,
      type: 'stream',
    }
  )

  if (fromCache) {
    return fromCache
  }

  const data = await action()

  // Too convenient?
  if (data instanceof Response) {
    if (data.body == null) {
      throw new Error('Invalid data')
    }
    const [streamForCache, streamForResponse] = data.body.tee()
    await ctx.bindings.tgmrankData.put(
      kvPath(ctx.bindings.ENVIRONMENT, key),
      streamForCache,
      {
        expirationTtl: expiration.putTtl,
      }
    )

    return streamForResponse
  }

  await ctx.bindings.tgmrankData.put(
    kvPath(ctx.bindings.ENVIRONMENT, key),
    data,
    {
      expirationTtl: expiration.putTtl,
    }
  )

  return data
}
