import { circularSlice, kvPath, readStream } from '@/utility'
import { Environment } from '@/types/Environment'

describe('utility', () => {
  describe('circularSlice', () => {
    it('should return an empty array when operating on an empty array', () => {
      expect(circularSlice([], 100, 100)).toHaveLength(0)
    })

    it('should slice zero elements', () => {
      expect(circularSlice([1, 2, 3], 100, 0)).toHaveLength(0)
    })

    it('should slice an array within bounds', () => {
      const array = [1, 2, 3, 4, 5]
      expect(circularSlice(array, 1, 3)).toStrictEqual([2, 3, 4])
    })

    it('should return a slice longer than array length', () => {
      const array = [1, 2, 3, 4, 5]
      expect(circularSlice(array, 1, 6)).toStrictEqual([2, 3, 4, 5, 1, 2])
    })

    it('should slice an array past bounds', () => {
      const array = [1, 2, 3, 4, 5]
      expect(circularSlice(array, 3, 3)).toStrictEqual([4, 5, 1])
    })

    it('should return an empty array when n is 0', () => {
      const array = [1, 2, 3, 4, 5]
      expect(circularSlice(array, 100, 0)).toStrictEqual([])
    })

    it('should adjust starting point when start index is large', () => {
      const array = [1, 2, 3, 4, 5]
      expect(circularSlice(array, 10001, 3)).toStrictEqual([2, 3, 4])
    })

    it('should adjust starting point when start index is negative', () => {
      const array = [1, 2, 3, 4, 5]
      expect(circularSlice(array, -1004, 3)).toStrictEqual([2, 3, 4])
    })
  })

  describe('readStream', () => {
    function encodeChunks(nonEncodedChunks: ReadonlyArray<string>) {
      const textEncoder = new TextEncoder()

      return nonEncodedChunks.map((value) => textEncoder.encode(value))
    }

    function buildMockStream(chunks: ReadonlyArray<Uint8Array>) {
      return {
        getReader() {
          let chunkIndex = 0

          return {
            read() {
              return Promise.resolve(
                chunkIndex < chunks.length
                  ? { value: chunks[chunkIndex++], done: false }
                  : { value: undefined, done: true }
              )
            },
          }
        },
      } as ReadableStream
    }

    it('should read empty stream', async () => {
      const chunks: ReadonlyArray<string> = []
      const stream = buildMockStream(encodeChunks(chunks))

      const result = await readStream(stream)
      expect(result).toBe('')
    })

    it('should read multi-chunk stream', async () => {
      const chunks = ['one', 'two', 'three']
      const stream = buildMockStream(encodeChunks(chunks))

      const result = await readStream(stream)
      expect(result).toBe('onetwothree')
    })

    it('should decode utf-8 data', async () => {
      const chunks = ['三原さん']
      const stream = buildMockStream(encodeChunks(chunks))

      const result = await readStream(stream)
      expect(result).toBe(chunks[0])
    })

    it('should decode utf-8 data across chunks', async () => {
      const chunks = [new Uint8Array([228, 184]), new Uint8Array([137])]
      const stream = buildMockStream(chunks)

      const result = await readStream(stream)
      expect(result).toBe('三')
    })
  })

  describe('kvPath', () => {
    it('should use $ as a delimiter instead of slashes', () => {
      expect(kvPath(Environment.dev, '///')).toBe('$$$')
      expect(kvPath(Environment.dev, '/hello/world/')).toBe('$hello$world$')
    })
  })
})
