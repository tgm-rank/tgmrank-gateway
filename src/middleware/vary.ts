import { Context } from '@/bindings'

export function varyHeader(ctx: Context) {
  ctx.defer((response) => {
    response.headers.set(
      'Vary',
      'Accept-Encoding,Origin,Access-Control-Request-Method,Access-Control-Request-Headers'
    )
  })
}
