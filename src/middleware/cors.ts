import { Context } from '@/bindings'
import { reply } from 'worktop/response'

const allowedMethods = 'PATCH,DELETE,GET,POST,PUT'
const allowedMethodsArray = ['PATCH', 'DELETE', 'GET', 'POST', 'PUT']

export function corsMiddleware(request: Request, ctx: Context) {
  if (request.method === 'OPTIONS') {
    const origin = request.headers.get('Origin')
    const allowedOrigins = ctx.bindings.CORS_ALLOWED_ORIGINS.split(';')

    if (origin == null || !allowedOrigins.includes(origin)) {
      return reply(400)
    }

    const method = request.headers.get('Access-Control-Request-Method')
    if (method == null || !allowedMethodsArray.includes(method.toUpperCase())) {
      return reply(400)
    }

    return new Response(null, {
      status: 204,
      headers: {
        ['Access-Control-Allow-Origin']: origin,
        ['Access-Control-Allow-Methods']: allowedMethods,
        ['Access-Control-Allow-Headers']: 'content-type, authorization, accept',
        ['Access-Control-Max-Age']: '7200',
        ['Access-Control-Allow-Credentials']: 'true',
      },
    })
  }

  ctx.defer((response) => {
    const origin = request.headers.get('Origin')
    if (origin != null) {
      response.headers.set('Access-Control-Allow-Origin', origin)
      response.headers.set('Access-Control-Allow-Credentials', 'true')
    }
  })
}
