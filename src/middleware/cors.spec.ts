import * as cors from './cors'
import { testEnv, createContext } from '../../test/utility'

const validOrigin = testEnv.CORS_ALLOWED_ORIGINS.split(';')[1]

const validCorsHeaders = {
  ['OrIgIn']: validOrigin,
  ['Access-Control-Request-Method']: 'GET',
}

describe('cors', () => {
  it('should respond with BadRequest when preflight request origin does not match', async () => {
    const req = new Request('https://some.bad.site/thing', {
      method: 'OPTIONS',
      headers: {
        ['OrIgIn']: 'https://some.bad.site',
      },
    })

    const response = cors.corsMiddleware(req, createContext())
    expect(response).toBeDefined()
    expect(response?.status).toBe(400)
  })

  it('should respond with BadRequest when preflight request method does not match', async () => {
    const req = new Request('https://some.bad.site/thing', {
      method: 'OPTIONS',
      headers: {
        ['OrIgIn']: 'https://my-other-origin.site',
        ['Access-Control-Request-Method']: 'ASDF',
      },
    })

    const response = cors.corsMiddleware(req, createContext())
    expect(response).toBeDefined()
    expect(response?.status).toBe(400)
  })

  it('should respond to preflight request with CORS headers when request is valid', async () => {
    const req = new Request(`${validOrigin}/thing`, {
      method: 'OPTIONS',
      headers: validCorsHeaders,
    })

    const response = cors.corsMiddleware(req, createContext())

    expect(response).toBeDefined()
    expect(response?.status).toBe(204)
    expect(response?.headers.get('Access-Control-Allow-Origin')).toBe(
      validOrigin
    )
    expect(response?.headers.get('Access-Control-Max-Age')).toBe('7200')
    expect(response?.headers.get('Access-Control-Allow-Credentials')).toBe(
      'true'
    )
    expect(response?.headers.get('Access-Control-Allow-Methods')).toBe(
      'PATCH,DELETE,GET,POST,PUT'
    )
    expect(response?.headers.get('Access-Control-Allow-Headers')).toBe(
      'content-type, authorization, accept'
    )
  })
})

it('should set headers for non-preflight requests', async () => {
  const req = new Request(`${validOrigin}/thing`, {
    method: 'GET',
    headers: validCorsHeaders,
  })

  const ctx = createContext()
  let response = cors.corsMiddleware(req, ctx)
  expect(response).toBeUndefined()

  response = new Response()
  const deferMock = ctx.defer as jest.Mock
  expect(deferMock).toHaveBeenCalledTimes(1)

  const deferFn = deferMock.mock.calls[0][0]
  deferFn(response)

  expect(response?.headers.get('Access-Control-Allow-Origin')).toBe(validOrigin)
  expect(response?.headers.get('Access-Control-Allow-Credentials')).toBe('true')
})
