import { varyHeader } from './vary'
import { createContext } from '../../test/utility'

describe('vary', () => {
  it('should set vary header', () => {
    const ctx = createContext()
    varyHeader(ctx)

    const response = new Response()
    const deferMock = ctx.defer as jest.Mock
    expect(deferMock).toHaveBeenCalledTimes(1)

    const deferFn = deferMock.mock.calls[0][0]
    deferFn(response)

    expect(response?.headers.get('Vary')).toBe(
      'Accept-Encoding,Origin,Access-Control-Request-Method,Access-Control-Request-Headers'
    )
  })
})
