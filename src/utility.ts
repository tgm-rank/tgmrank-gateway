import { Environment } from './types/Environment'

export function circularSlice<T>(
  array: ReadonlyArray<T>,
  start: number,
  n: number
): ReadonlyArray<T> {
  if (array.length === 0) {
    return []
  }

  const slice: Array<T> = []

  start = ((start % array.length) + array.length) % array.length
  for (let i = 0; i < n; ++i) {
    const element = array[(start + i) % array.length]
    slice.push(element)
  }

  return slice
}

export function jsonResponse(status: number, body: BodyInit): Response {
  return new Response(body, {
    status: status,
    headers: {
      ['Content-Type']: 'application/json',
    },
  })
}

const textDecoder = new TextDecoder('utf-8')
export async function readStream(stream: ReadableStream): Promise<string> {
  const reader = stream.getReader()

  let result = ''
  let chunk = null
  do {
    chunk = await reader.read()

    if (chunk.value) {
      result += textDecoder.decode(chunk.value, {
        stream: true,
      })
    }
  } while (!chunk.done)

  return result + textDecoder.decode()
}

export function kvPath(environment: Environment, path: string) {
  if (environment != Environment.prod) {
    return path.replaceAll('/', '$')
  }

  return path
}
