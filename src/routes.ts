import { reply } from 'worktop/response'

import PromisePool from '@supercharge/promise-pool'
import { GameRankingModel } from '@tgmrank/types'
import { Router } from 'worktop'
import {
  readOrCache,
  defaultExpiration,
  Expiration,
  storeResponse,
} from './api'
import { Context } from './bindings'
import { jsonResponse, kvPath, readStream } from './utility'

import * as FightcadeService from './services/fightcade'

function isQueryStringEmpty(url: string) {
  return new URL(url).search?.length === 0
}

async function readOrFetch(
  ctx: Context,
  path: string,
  expiration: Expiration = defaultExpiration
): Promise<ReadableStream | null> {
  return (
    (await ctx.bindings.tgmrankData.get(
      kvPath(ctx.bindings.ENVIRONMENT, path),
      {
        cacheTtl: expiration.getTtl,
        type: 'stream',
      }
    )) ?? (await storeResponse(ctx, path, expiration.putTtl))
  )
}

function addCacheRoutes(router: Router<Context>) {
  router.add('POST', '/cache', async (request, ctx) => {
    const text = await request.text()
    const reqObject = JSON.parse(text)
    const paths: string[] = reqObject.paths ?? []
    ctx.waitUntil(
      PromisePool.withConcurrency(3)
        .for(paths)
        .process(async (path: string) =>
          storeResponse(ctx, path, defaultExpiration.putTtl)
        )
    )
    return jsonResponse(
      200,
      JSON.stringify({
        processing: true,
      })
    )
  })
  router.add('POST', '/purge', async (request, ctx) => {
    const text = await request.text()
    const reqObject = JSON.parse(text)
    const paths: string[] = reqObject.paths ?? []
    ctx.waitUntil(
      PromisePool.withConcurrency(3)
        .for(paths)
        .process(async (path: string) => ctx.bindings.tgmrankData.delete(path))
    )
    return jsonResponse(
      200,
      JSON.stringify({
        processing: true,
      })
    )
  })
}

export function registerRoutes(router: Router<Context>) {
  router.add('GET', '/robots.txt', () => {
    return reply(200, `Agent: *\nDisallow: /`)
  })

  addCacheRoutes(router)

  router.add('GET', '/v1/game', async function (request, ctx) {
    if (!isQueryStringEmpty(request.url)) {
      return reply(400, { message: 'Unexpected query string parameters' })
    }

    const data = await readOrFetch(
      ctx,
      new URL(request.url).pathname.toLowerCase(),
      {
        ...defaultExpiration,
        getTtl: 600,
      }
    )
    if (data == null) {
      return reply(404, { message: 'Not found' })
    }

    const response = jsonResponse(200, data)
    response.headers.set('Cache-Control', 'max-age=600,s-maxage=600')
    return response
  })

  router.add(
    'GET',
    '/v1/game/:gameId/:rankingKey/ranking',
    async (request, ctx) => {
      const data = await readOrFetch(
        ctx,
        new URL(request.url).pathname.toLowerCase()
      )
      if (data == null) {
        return reply(404, { message: 'Not found' })
      }

      const response = jsonResponse(200, data)
      response.headers.set('Cache-Control', 'max-age=30')
      return response
    }
  )

  router.add(
    'GET',
    '/v1/game/:gameId/:rankingKey/ranking/player/:playerId',
    async (request, ctx) => {
      const data = await readOrFetch(
        ctx,
        `/v1/game/${ctx.params.gameId}/${ctx.params.rankingKey}/ranking`.toLowerCase()
      )
      if (data == null) {
        return reply(404, { message: 'Not found' })
      }

      const rankingDataString = await readStream(data)

      const json: Array<GameRankingModel> = JSON.parse(rankingDataString)
      const filteredData = json.filter(
        (r: GameRankingModel) =>
          r.player.playerId === Number(ctx.params.playerId)
      )

      const response = jsonResponse(200, JSON.stringify(filteredData))
      response.headers.set('Cache-Control', 'max-age=30')
      return response
    }
  )

  router.add('GET', '/v1/location', async (request, ctx) => {
    const data = await readOrFetch(
      ctx,
      new URL(request.url).pathname.toLowerCase()
    )
    if (data == null) {
      return reply(404, { message: 'Not found' })
    }

    const response = jsonResponse(200, data)

    const twoWeeksInSeconds = 60 * 60 * 24 * 14
    response.headers.set('Cache-Control', `max-age=${twoWeeksInSeconds}`)
    return response
  })

  router.add('GET', '/v1/mode/:modeId/ranking', async (request, ctx) => {
    const data = await readOrFetch(
      ctx,
      new URL(request.url).pathname.toLowerCase()
    )
    if (data == null) {
      return reply(404, { message: 'Not found' })
    }

    const response = jsonResponse(200, data)
    response.headers.set('Cache-Control', 'max-age=30')
    return response
  })

  router.add('GET', '/v1/grade/mode/:modeId', async (request, ctx) => {
    const data = await readOrFetch(
      ctx,
      new URL(request.url).pathname.toLowerCase()
    )
    if (data == null) {
      return reply(404, { message: 'Not found' })
    }

    const response = jsonResponse(200, data)
    response.headers.set('Cache-Control', 'max-age=30')
    return response
  })

  router.add('GET', '/v1/player/:playerId/scores', async (request, ctx) => {
    const data = await readOrFetch(
      ctx,
      new URL(request.url).pathname.toLowerCase()
    )
    if (data == null) {
      return reply(404, { message: 'Not found' })
    }

    const response = jsonResponse(200, data)
    response.headers.set('Cache-Control', 'max-age=30')
    return response
  })

  router.add('GET', '/v1/score/activity', async (request, ctx) => {
    const url = new URL(request.url)
    const data = await readOrFetch(
      ctx,
      url.pathname.toLowerCase() + url.search,
      {
        ...defaultExpiration,
        putTtl: 60,
      }
    )
    if (data == null) {
      return reply(404, { message: 'Not found' })
    }

    return jsonResponse(200, data)
  })

  router.add('GET', '/v1/fightcade/ranking', async (request, ctx) => {
    const data = await readOrCache(
      ctx,
      '/v1/fightcade/ranking',
      () =>
        FightcadeService.getLeaderboard(ctx).then((data) =>
          JSON.stringify(data)
        ),
      {
        putTtl: 3600,
        getTtl: 3600,
      }
    )

    if (data == null) {
      return reply(404, { message: 'Not found' })
    }

    const response = jsonResponse(200, data)
    response.headers.set('Cache-Control', 'max-age=3600')

    return response
  })

  router.add('GET', '*', () => reply(404, { message: 'Not found' }))
  router.add('POST', '*', () => reply(404, { message: 'Not found' }))
}
